var logger = require("tracer").colorConsole();
const express = require("express");
const bodyParser = require("body-parser");
const houseroutes = require("./src/routes/house.routes");
const mealroutes = require("./src/routes/meal.routes");
const authroutes = require("./src/routes/authentication.routes");
const participantsroutes = require("./src/routes/participants.routes");
const app = express();

app.use(bodyParser.json());
const port = process.env.PORT || 3000;

app.all("*", (req, res, next) => {
  const method = req.method;
  logger.log("Method :", method);
  next();
});
// The application routes
app.use("/api", participantsroutes);
app.use("/api/studenthome", houseroutes);
app.use("/api/studenthome", mealroutes);
app.use("/api", authroutes);
//  Run if an endpoint doesn't exist
app.all("*", (req, res, next) => {
  logger.log("Wrong endpoint");
  res.status(404).json({
    error: "Endpoint does not exist!",
  });
});

app.listen(port, () => logger.log(`Example app listening at port ${port}`));
module.exports = app;
