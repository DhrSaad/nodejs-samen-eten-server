const assert = require("assert");
const jwt = require("jsonwebtoken");
const pool = require("../config/database");
var logger = require("tracer").colorConsole();
let controller = {
  addMeal(req, res, next) {
    logger.log("addMeal Info function called");
    // Variables loaded
    let houseid = req.param("houseid");
    let name = req.body.name;
    let description = req.body.description;
    let ingredients = req.body.ingredients;
    let allergies = req.body.allergies;
    let createdon = new Date().toISOString().slice(0, 19).replace("T", " ");
    let offerdon = new Date().toISOString().slice(0, 19).replace("T", " ");
    let price = req.body.price;
    let studenthomeid = req.body.studenthomeid;
    let maxparticipants = req.body.maxparticipants;
    let userid = req.body.userid;

    pool.getConnection((err, connection) => {
      // If connection is broken throw Error
      if (err) {
        logger.error("Error getting connection from pool");
        res
          .status(500)
          .json({ error: err.toString(), datetime: new Date().toISOString() });
      }
      // If connection is succesfull run
      if (connection) {
        logger.error("Connection to database is established");
        // Set the query and run it
        connection.query(
          "INSERT INTO `meal` (`Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudenthomeID`, `MaxParticipants`) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
          [
            name,
            description,
            ingredients,
            allergies,
            createdon,
            offerdon,
            price,
            userid,
            studenthomeid,
            maxparticipants,
          ],
          (err, rows, fields) => {
            connection.release();
            if (err) {
              logger.error("Error: ", err.toString());
              res.status(500).json({
                error: err.toString(),
              });
            } else {
              res.status(200).json({
                result: req.body,
                message: "Meal created",
              });
            }
          }
        );
      }
    });
  },
  getMeal(req, res, next) {
    logger.log("Called getHouses function");
    let homeid = req.param("homeid");
    // Get connection from pool
    pool.getConnection((err, connection) => {
      // If pool gets connection run this
      if (err) {
        logger.error("Error getting connection from pool");
        res.status(500).json({ error: err.toString() });
      }
      // Select query run after connection has been established
      if (connection) {
        logger.log("StudenthomeID", homeid);
        connection.query(
          "SELECT * FROM `meal` WHERE `StudenthomeID` = ?",
          [homeid],

          (err, rows, fields) => {
            connection.release();
            // If there is an error run this code
            if (err) {
              logger.error("Error: ", err.toString());
              res.status(500).json({
                error: err.toString(),
              });
            }
            // Else run this code if SQL has been succesful
            else {
              logger.info("Result from database: ");
              logger.info(rows);
              res.status(200).json({
                result: rows,
              });
            }
          }
        );
      }
    });
  },
  deleteMeal(req, res, next) {
    let homeid = req.param("homeid");
    let mealid = req.param("mealid");
    let userid = req.body.userid;
    // Get connection from pool
    pool.getConnection((err, connection) => {
      if (err) {
        logger.error("Error getting connection from pool");
        res.status(500).json({ error: err.toString() });
      }
      // Select query run after connection has been established
      if (connection) {
        connection.query(
          "DELETE FROM `meal` WHERE `ID` = ? AND `StudenthomeID` = ? AND `UserID` = ?",
          [mealid, homeid, userid],
          (err, rows, fields) => {
            connection.release();
            //  SQL error throw
            if (err) {
              logger.error("Error: ", err.toString());
              res.status(500).json({
                error: err.toString(),
              });
            }
            // Run if everything went well
            else {
              logger.info("Deleted studenthome");
              logger.info(rows);
              res.status(200).json({
                result: rows,
                message: "Meal deleted",
              });
            }
          }
        );
      }
    });
  },
  EditMeal(req, res, next) {
    // All body variables
    let houseid = req.param("homeid");
    let mealid = req.param("mealid");
    let name = req.body.name;
    let description = req.body.description;
    let ingredients = req.body.ingredients;
    let allergies = req.body.allergies;

    let price = req.body.price;
    let maxparticipants = req.body.maxparticipants;
    let userid = req.body.userid;
    let createdon = new Date().toISOString().slice(0, 19).replace("T", " ");
    let offerdon = new Date().toISOString().slice(0, 19).replace("T", " ");
    // Get connection from pool and load config
    pool.getConnection((err, connection) => {
      // If connection error then throw error
      if (err) {
        logger.error("Error getting connection from pool");
        res
          .status(500)
          .json({ error: err.toString(), datetime: new Date().toISOString() });
      }
      // If connection is succesfull run
      if (connection) {
        connection.query(
          "UPDATE `meal` SET `Name` = ?, `Description` = ?, `Ingredients` = ?, `Allergies` = ?, `Price` = ?, MaxParticipants = ? WHERE `ID` = ? AND `StudenthomeID` = ? AND `UserID` = ?",
          [
            name,
            description,
            ingredients,
            allergies,

            price,
            maxparticipants,
            mealid,
            houseid,
            userid,
          ],
          (err, rows, fields) => {
            connection.release();
            // If sql throws an error run this
            if (err) {
              logger.error("Error: ", err.toString());
              res.status(500).json({
                error: err.toString(),
              });
            } else {
              // If data has succesfully changed send message en close.
              logger.info("Succesfully Changed dataset");
              res.status(200).json({
                result: req.body,
                message: "Meal updated!",
              });
            }
          }
        );
      }
    });
  },
  searchMeal(req, res, next) {
    // Get variable out of query
    let homeid = req.param("homeid");
    let mealid = req.param("mealid");
    // Get connection from pool
    pool.getConnection((err, connection) => {
      // If pool gets connection run this
      if (err) {
        logger.error("Error getting connection from pool");
        res.status(500).json({ error: err.toString() });
      }
      // Select query run after connection has been established
      if (connection) {
        connection.query(
          "SELECT * FROM `meal` WHERE `ID` = ? AND `StudenthomeID` = ?",
          [mealid, homeid],
          (err, rows, fields) => {
            connection.release();
            // If there is an error run this code
            if (err) {
              logger.error("Error: ", err.toString());
              res.status(500).json({
                error: err.toString(),
              });
            }
            // Else run this code if SQL has been succesful
            else {
              logger.info("Result from database: ");
              logger.info(rows);
              res.status(200).json({
                result: rows,
              });
            }
          }
        );
      }
    });
  },
};
module.exports = controller;
