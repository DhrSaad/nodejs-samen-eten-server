const assert = require("assert");
const jwt = require("jsonwebtoken");
const pool = require("../config/database");
const logger = require("../config/config").logger;

// De twee gebruikte RegEx
const regPhonenumber = new RegExp(/\d\d\d\d\d\d\d\d\d\d/);
const regPostcode = new RegExp(/\d\d\d\d[a-zA-Z]+[a-zA-Z]/);
// Controller object
let controller = {
  EditHouse(req, res, next) {
    if (!req.param("homeid")) {
      res.status(412).json({
        result: {
          error: "Item not found of you do not have access to this item",
        },
      });
    }
    // All body variables
    var houseID = req.param("homeid");
    var editstreet = req.body.address;
    var editpostcode = req.body.postcode;
    var edithn = req.body.housenumber;
    var editphonenumber = req.body.phonenumber;
    var editname = req.body.name;
    var editcity = req.body.city;
    var userid = req.body.userid;

    // Assert if the input is correct or not. throws assertion error

    // Get connection from pool and load config
    pool.getConnection((err, connection) => {
      // If connection error then throw error
      if (err) {
        logger.error("Error getting connection from pool");
        res
          .status(500)
          .json({ error: err.toString(), datetime: new Date().toISOString() });
      }
      // If connection is succesfull run
      if (connection) {
        connection.query(
          "UPDATE `studenthome` SET `Name` = ?, `Address` = ?, `House_Nr` = ?, `Postal_Code` = ?, `Telephone` = ?, `City` = ? WHERE `ID` = ? AND `UserID` = ?",
          [
            editname,
            editstreet,
            edithn,
            editpostcode,
            editphonenumber,
            editcity,
            houseID,
            userid,
          ],
          (err, rows, fields) => {
            connection.release();
            // If sql throws an error run this
            if (err) {
              logger.error("Error: ", err.toString());
              res.status(500).json({
                error: err.toString(),
              });
            } else if (rows.affectedRows === 0) {
              logger.trace("item was NOT updated");
              res.status(401).json({
                error: "Item not found of you do not have access to this item",
              });
            } else {
              // If data has succesfully changed send message en close.
              logger.info("Succesfully Changed dataset");
              res.status(200).json({
                result: req.body,
                message: "Updated studenthome",
              });
            }
          }
        );
      }
    });
  },

  AddHouse(req, res, next) {
    // Alle RegEx inladen

    // Alle variablen uit de parameters halen
    let name = req.body.name;
    let address = req.body.address;
    let hn = req.body.housenumber;
    let phnnumber = req.body.phonenumber;
    let postcode = req.body.postcode;
    let city = req.body.city;
    let userid = req.body.userid;

    // Controlling parameters & Checking if input is correct by regex

    // Create connection with SQL database
    pool.getConnection((err, connection) => {
      if (err) {
        logger.error("Error getting connection from pool");
        res.status(500).json({ error: err.toString() });
      }
      // If connected then this happens
      if (connection) {
        logger.log("Adding home object to database, Object : ", name);
        connection.query(
          "INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES(?, ?, ?, ?, ?, ?, ?)",
          [name, address, hn, userid, postcode, phnnumber, city],
          (err, rows, fields) => {
            connection.release();
            // Throw error if something happens to connection with database
            if (err) {
              logger.error("Error: ", err.toString());
              res.status(500).json({
                error: err.toString(),
              });
            }
            // Run if everything went smoothly
            else {
              res.status(200).json({
                result: req.body,
                message: "created home succesfully",
              });
            }
          }
        );
      }
    });
  },

  GetAllHouses(req, res, next) {
    // Variables out of the query
    let city = req.query.city;
    let name = req.query.name;
    // If connection go ahead else throw error
    pool.getConnection((err, connection) => {
      if (err) {
        logger.error("Error getting connection from pool");
        res.status(500).json({ error: err.toString() });
      }

      if (connection) {
        // If city is used run this

        connection.query(
          "SELECT * FROM `studenthome`",

          (err, rows, fields) => {
            connection.release();
            if (err) {
              logger.error("Error: ", err.toString());
              res.status(500).json({
                error: err.toString(),
              });
            } else {
              // Run after
              logger.info("Result from database: ");
              logger.info(rows);
              res.status(200).json({
                result: rows,
              });
            }
          }
        );
      }
    });
  },
  validatehouse(req, res, next) {
    // Verify that we receive the expected input
    try {
      assert(
        typeof req.body.phonenumber === "string",
        "phonenumber must be a number and/or be filled in."
      );
      assert(
        typeof req.body.phonenumber === "string",
        "phonenumber must be a number and/or be filled in."
      );
      next();
    } catch (ex) {
      res.status(422).json({ error: ex.toString() });
    }
  },
  searchhouse(req, res, next) {
    // Get variable out of query
    let homeid = req.param("homeid");
    // Get connection from pool
    pool.getConnection((err, connection) => {
      // If pool gets connection run this
      if (err) {
        logger.error("Error getting connection from pool");
        res.status(500).json({ error: err.toString() });
      }
      // Select query run after connection has been established
      if (connection) {
        connection.query(
          "SELECT * FROM `studenthome` WHERE `ID` = ?",
          [homeid],
          (err, rows, fields) => {
            connection.release();
            // If there is an error run this code
            if (err) {
              logger.error("Error: ", err.toString());
              res.status(500).json({
                error: err.toString(),
              });
            }
            // Else run this code if SQL has been succesful
            if (rows.length <= 0) {
              res.status(412).json({
                error: "Something went wrong. User not found",
              });
            } else {
              logger.info("Result from database: ");
              logger.info(rows);
              res.status(200).json({
                result: rows,
              });
            }
          }
        );
      }
    });
  },
  delHouse(req, res, next) {
    let homeid = req.param("homeid");
    let userid = req.body.userid;
    // Get connection from pool
    pool.getConnection((err, connection) => {
      if (err) {
        logger.error("Error getting connection from pool");
        res.status(500).json({ error: err.toString() });
      }
      // Select query run after connection has been established
      if (connection) {
        connection.query(
          "DELETE FROM `studenthome` WHERE `ID` = ? AND `UserID` = ?",
          [homeid, userid],
          (err, rows, fields) => {
            connection.release();
            //  SQL error throw
            if (err) {
              logger.error("Error: ", err.toString());
              res.status(500).json({
                error: err.toString(),
              });
            } else if (rows.affectedRows === 0) {
              logger.trace("item was NOT deleted");
              res.status(401).json({
                error: "Item not found of you do not have access to this item",
              });
            }
            // Run if everything went well
            else {
              logger.info("Deleted studenthome");
              logger.info(rows);
              res.status(200).json({
                result: rows,
                message: "Deleted studenthome",
              });
            }
          }
        );
      }
    });
  },
  addUser(req, res, next) {
    // Get variables out of query parameters
    let userid = req.body.userid;
    let homeid = req.param("homeid");
    pool.getConnection((err, connection) => {
      // If pool gets an error run this
      if (err) {
        logger.error("Error getting connection from pool");
        res.status(500).json({ error: err.toString() });
      }
      // If connection is established then run this query
      if (connection) {
        connection.query(
          "UPDATE `studenthome` SET `UserID` = ? WHERE `ID` = ?",
          [userid, homeid],
          (err, rows, fields) => {
            connection.release();
            // If sql throws an error show on screen
            if (err) {
              logger.error("Error: ", err.toString());
              res.status(500).json({
                error: err.toString(),
              });
            } else {
              // Else run this
              logger.info("Added User ID");
              logger.info(rows);
              res.status(200).json({
                result: rows,
              });
            }
          }
        );
      }
    });
  },
};
module.exports = controller;
