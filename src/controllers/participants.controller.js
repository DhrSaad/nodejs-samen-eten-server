const pool = require("../config/database");
const config = require("../config/config");
const logger = config.logger;

let controller = {
  signUp(req, res, next) {
    const today = new Date().toISOString().slice(0, 10);
    const homeId = req.params.homeId;
    const mealId = req.params.mealId;
    const userId = req.userId;
    logger.debug("homeId: ", homeId);
    logger.debug("mealId: ", mealId);
    logger.debug("userId: ", userId);
    logger.debug("signedUpOn: ", today);
    logger.info(
      `POST aangeroepen op /api/studenthome/${homeId}/meal/${mealId}/signup`
    );

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          message: "signUp failed!",
          error: err,
        });
      }

      if (connection) {
        let sqlQuery =
          "INSERT INTO `participants` (`UserID`, `StudenthomeID`, `MealID`, `SignedUpOn`) VALUES (?, ?, ?, ?)";
        logger.info("signUp", "sqlQuery =", sqlQuery);

        connection.query(
          sqlQuery,
          [userId, homeId, mealId, today],
          (error, result, fields) => {
            connection.release();

            if (error) {
              res.status(400).json({
                message: "signUp failed!",
                error: err,
              });
            }

            if (result) {
              if (result.affectedRows === 0) {
                res.status(404).json({
                  message: "No meal/user selected!",
                });
              }

              if (result.affectedRows > 0) {
                res.status(200).json({
                  result: result,
                  message: "Signup Succesful",
                });
              }
            }
          }
        );
      }
    });
  },

  signOff(req, res, next) {
    const homeId = req.params.homeId;
    const mealId = req.params.mealId;
    const userId = req.userId;
    logger.debug("homeId: ", homeId);
    logger.debug("mealId: ", mealId);
    logger.debug("userId: ", userId);
    logger.info(
      `PUT aangeroepen op /api/studenthome/${homeId}/meal/${mealId}/signoff`
    );

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          message: "signUp failed!",
          error: err,
        });
      }

      if (connection) {
        let sqlQuery =
          "DELETE FROM `participants` WHERE `UserID` = ? AND `StudenthomeID` = ? AND `MealID` = ?";
        logger.info("signOff", "sqlQuery =", sqlQuery);

        connection.query(
          sqlQuery,
          [userId, homeId, mealId],
          (error, result, fields) => {
            connection.release();

            if (error) {
              res.status(400).json({
                message: "signOff failed!",
                error: err,
              });
            }

            if (result) {
              if (result.affectedRows === 0) {
                res.status(400).json({
                  message: "signoff failed! there are no participants",
                });
              }
              if (result.affectedRows > 0) {
                res.status(200).json({
                  result: result,
                  message: "Sign off succesfull",
                });
              }
            }
          }
        );
      }
    });
  },

  allParticipants(req, res, next) {
    const mealId = req.params.mealId;
    const userId = req.userId;
    logger.debug("mealId: ", mealId);
    logger.debug("userId: ", userId);
    logger.info(`GET aangeroepen op /api/meal/${mealId}/participants`);

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          message: "signUp failed!",
          error: err,
        });
      }

      if (connection) {
        let sqlQuery = "SELECT * FROM `participants` WHERE `MealID` = ?";
        logger.info("signOff", "sqlQuery =", sqlQuery);

        connection.query(sqlQuery, [mealId], (error, result, fields) => {
          connection.release();

          if (error) {
            res.status(400).json({
              message: "Have u checked if there are any participants?",
              error: err,
            });
          }

          if (result) {
            if (result.length === 0) {
              res.status(400).json({
                message: "No Participants!",
              });
            }
            if (result.length > 0) {
              res.status(200).json({
                result: result,
              });
            }
          }
        });
      }
    });
  },

  participantDetails(req, res, next) {
    const mealId = req.params.mealId;
    const participantId = req.params.participantId;
    const userId = req.userId;
    logger.debug("mealId: ", mealId);
    logger.debug("userId: ", userId);
    logger.info(
      `GET aangeroepen op /api/meal/${mealId}/participants/${participantId}`
    );

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          message: "signUp failed!",
          error: err,
        });
      }

      if (connection) {
        let sqlQuery =
          "SELECT * FROM `participants` WHERE `MealID` = ? AND `UserID` = ?";

        connection.query(
          sqlQuery,
          [mealId, participantId],
          (error, result, fields) => {
            connection.release();

            if (error) {
              res.status(400).json({
                message: "getParticipantDetails failed! Error with MYSQL",
                error: err,
              });
            }

            if (result) {
              if (result.length === 0) {
                res.status(400).json({
                  message: "No participants here",
                });
              }
              if (result.length > 0) {
                res.status(200).json({
                  result: result,
                });
              }
            }
          }
        );
      }
    });
  },
};

module.exports = controller;
