const routes = require("express").Router();
const AuthController = require("../controllers/user.controller");
const ParticipantsController = require("../controllers/participants.controller");

routes.post(
  "/studenthome/:homeId/meal/:mealId/signup",
  AuthController.validateToken,
  ParticipantsController.signUp
);
routes.put(
  "/studenthome/:homeId/meal/:mealId/signoff",
  AuthController.validateToken,
  ParticipantsController.signOff
);
routes.get(
  "/meal/:mealId/participants",
  ParticipantsController.allParticipants
);
routes.get(
  "/meal/:mealId/participants/:participantId",
  AuthController.validateToken,
  ParticipantsController.participantDetails
);

module.exports = routes;
