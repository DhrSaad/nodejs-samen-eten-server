const express = require("express");
const { validatehouse } = require("../controllers/house.controller");
const router = express.Router();
const housecontroller = require("../controllers/house.controller");
const authcontroller = require("../controllers/user.controller");
var logger = require("tracer").colorConsole();
// house routes

// Post functie om een nieuwe studentenhuis toe te voegen aan het database systeem
router.post(
  "/",
  authcontroller.validateToken,
  housecontroller.validatehouse,
  housecontroller.AddHouse
);
// get all houses functions
router.get("/", housecontroller.GetAllHouses);
// Route voor het zoeken van een huis
router.get("/:homeid", housecontroller.searchhouse);
// Wijzigen van de studentenhuizen
router.put(
  "/:homeid",
  housecontroller.validatehouse,
  authcontroller.validateToken,
  housecontroller.EditHouse
);
// Route voor het verwijderen van een huis
router.delete(
  "/:homeid",
  authcontroller.validateToken,
  housecontroller.delHouse
);
// Gebruiker toevoegen aan studentenhuis
router.put(
  "/:homeid/user/",
  authcontroller.validateToken,
  housecontroller.addUser
);

module.exports = router;
