const express = require("express");
const router = express.Router()
const mealcontroller = require('../controllers/meal.controller')
const authcontroller = require('../controllers/user.controller')
var logger = require('tracer').colorConsole()


//  Add meal to db route
router.post('/:homeid/meal',  authcontroller.validateToken, mealcontroller.addMeal)
// Route for list met meals
 router.get('/:homeid/meal', mealcontroller.getMeal)
 // Delete meal route
 router.delete('/:homeid/meal/:mealid', authcontroller.validateToken, mealcontroller.deleteMeal)
// Route om de update te weergeven
router.put('/:homeid/meal/:mealid', authcontroller.validateToken, mealcontroller.EditMeal)

// Detail opvragen van een Meal
router.get('/:homeid/meal/:mealid', mealcontroller.searchMeal)








module.exports = router;
