const tracer = require("tracer");
const server = require("../../index");
const pool = require("../config/database");
const logger = require("../config/config").logger;
const chai = require("chai");
const chaiHttp = require("chai-http");
const jwt = require("jsonwebtoken");

chai.should();
chai.use(chaiHttp);
tracer.setLevel("error");

const INSERT_MEALS =
  "INSERT INTO `meal` (`ID`, `Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudenthomeID`) VALUES " +
  "(3, 'Zuurkool met worst', 'Zuurkool a la Montizaan, specialiteit van het huis.', 'Zuurkool, worst, spekjes', 'Lactose, gluten','2020-09-01','2020-09-01', 5, 7, 7)," +
  "(4, 'Spaghetti', 'Spaghetti Bolognese', 'Pasta, tomatensaus, gehakt', 'Lactose','2020-09-01','2020-09-01', 3, 7, 8);";

const INSERT_STUDENTHOMES =
  "INSERT INTO `studenthome` (`ID`, `Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES " +
  "('7', 'Princenhage1', 'Princenhage', 11, 7,'4706RX','0612345678','Breda'), " +
  "('8', 'Haagdijk 231', 'Haagdijk', 4, 7, '4706RX','0612345678','Breda'), " +
  "('9', 'Den Hout1', 'Lovensdijkstraat', 61, 7, '4706RX','0612345678','Den Hout'), " +
  "('10', 'Den Dijk1', 'Langendijk', 63, 7, '4706RX','0612345678','Breda'), " +
  "('11', 'Lovensdijk1', 'Lovensdijkstraat', 62, 7, '4706RX','0612345678','Breda'), " +
  "('12', 'Goirkeplace', 'Goirkestraat', 23, 8, '4706RX','0612345678','Tilburg');";

const INSERT_USERS =
  "INSERT INTO `user` (`ID`, `First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES" +
  '(7, "first", "last", "test7@test7.nl","1234567", "secret"), ' +
  '(8, "first1", "last1", "test8@test8.nl","1234567", "secret");';

const CLEAR_STUDENTHOMES = "DELETE IGNORE FROM `studenthome`;";
const CLEAR_USERS = "DELETE IGNORE FROM `user`;";
const CLEAR_MEALS = "DELETE IGNORE FROM `meal`";

describe("Meals", () => {
  before((done) => {
    pool.query(CLEAR_MEALS, (err, rows) => {
      if (err) {
        done(err);
      }
      if (rows) {
        pool.query(CLEAR_STUDENTHOMES, (err, rows) => {
          if (err) {
            done(err);
          }
          if (rows) {
            pool.query(CLEAR_USERS, (err, rows) => {
              if (err) {
                done(err);
              }
              if (rows) {
                pool.query(INSERT_USERS, (err, rows) => {
                  if (err) {
                    done(err);
                  }
                  if (rows) {
                    pool.query(INSERT_STUDENTHOMES, (err, rows) => {
                      if (err) {
                        done(err);
                      }
                      if (rows) {
                        done();
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  });

  after((done) => {
    pool.query(CLEAR_MEALS, (err, rows) => {
      if (err) {
        done(err);
      }
      if (rows) {
        pool.query(CLEAR_STUDENTHOMES, (err, rows) => {
          if (err) {
            done(err);
          }
          if (rows) {
            pool.query(CLEAR_USERS, (err, rows) => {
              if (err) {
                done(err);
              }
              if (rows) {
                pool.query(INSERT_USERS, (err, rows) => {
                  if (err) {
                    done(err);
                  }
                  if (rows) {
                    pool.query(INSERT_STUDENTHOMES, (err, rows) => {
                      if (err) {
                        done(err);
                      }
                      if (rows) {
                        pool.query(INSERT_MEALS, (err, rows) => {
                          if (err) {
                            done(err);
                          }
                          if (rows) {
                            done();
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  });
  describe("UC-301 Create meal", () => {
    beforeEach((done) => {
      pool.query(CLEAR_MEALS, (err, rows) => {
        if (err) {
          done(err);
        }
        if (rows) {
          done();
        }
      });
    });

    afterEach((done) => {
      pool.query(CLEAR_MEALS, (err, rows) => {
        if (err) {
          done(err);
        }
        if (rows) {
          done();
        }
      });
    });

    it("(TC-301-1) heeft niet alle input", (done) => {
      chai
        .request(server)
        .post("/api/studenthome/7/meal")
        .set("authorization", "Bearer " + jwt.sign({ id: 5 }, "secret"))
        .send({
          "Description": "Description",
          "Price": 1,
        })
        .end((err, res) => {
          res.should.have.status(500);
          done();
        });
    });

    it("(TC-301-2) Gebruiker niet ingelogd", (done) => {
      chai
        .request(server)
        .post("/api/studenthome/7/meal")
        .send({
          name: "Name",
          description: "Description",
          ingredients: "Ingredients",
          allergies: "Allergies",
          offeredon: "2021-09-05",
          price: 1,
        })
        .end((err, res) => {
          res.should.have.status(401);
          res.body.should.be.a("object");
          done();
        });
    });

    it("(TC-301-3) Maaltijd niet aangemaakt", (done) => {
      chai
        .request(server)
        .post("/api/studenthome/7/meal")
        .set("authorization", "Bearer " + jwt.sign({ id: 7 }, "secret"))
        .send({
          name: "Name",
          description: "Description",
          ingredients: "Ingredients",
          allergies: "Allergies",
          offeredn: "2021-09-05",
          price: 1,
        })
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a("object");
          done();
        });
    });
  });

  describe("UC-302 Update meal", () => {
    beforeEach((done) => {
      pool.query(CLEAR_MEALS, (err, rows) => {
        if (err) {
          done(err);
        }
        if (rows) {
          pool.query(INSERT_MEALS, (err, rows) => {
            if (err) {
              done(err);
            }
            if (rows) {
              done();
            }
          });
        }
      });
    });

    afterEach((done) => {
      pool.query(CLEAR_MEALS, (err, rows) => {
        if (err) {
          done(err);
        }
        if (rows) {
          done();
        }
      });
    });

    it("(TC-302-1) Niet alle input", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/7/meal/")
        .set(
          "authorization",
          "Bearer " + jwt.sign({ email: "test@test.nl" }, "secret")
        )
        .end((err, res) => {
          res.should.have.status(404);
          res.body.should.be.a("object");
          done();
        });
    });

    it("(TC-302-2) Gebruiker niet ingelogd", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/7/meal/3")
        .send({
          name: "Name",
          description: "Description",
          ingredients: "Ingredients",
          allergies: "Allergies",
          offeredon: "9-5-2020",
          price: 1,
        })
        .end((err, res) => {
          res.should.have.status(401);
          res.body.should.be.a("object");
          done();
        });
    });

    it("(TC-302-3) Check of de gebruiker eigenaar is van de data", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/7/meal/3")
        .set(
          "authorization",
          "Bearer " + jwt.sign({ email: "test1@avans.nl" }, "password")
        )
        .send({
          name: "Name",
          description: "Description",
          ingredients: "Ingredients",
          allergies: "Allergies",
          offeredon: "9-5-2020",
          price: 1,
        })
        .end((err, res) => {
          res.should.have.status(401);
          res.body.should.be.a("object");
          done();
        });
    });

    it("(TC-302-4) Check Maaltijd wel bestaat", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/7/meal/1099")
        .set(
          "authorization",
          "Bearer " + jwt.sign({ email: "test@test.nl" }, "secret")
        )
        .send({
          name: "Meal1",
          description: "Description",
          ingredients: "Ingredients",
          allergies: "Allergies",
          offeredon: "9-5-2020",
          price: 1,
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });

    it("(TC-302-5) Maaltijd succesful geupdated", (done) => {
      chai
        .request(server)
        .put(`/api/studenthome/7/meal/3`)
        .set("authorization", "Bearer " + jwt.sign({ id: 7 }, "secret"))
        .send({
          name: "DifferentAndChangedName",
          description: "Description",
          ingredients: "Ingredients",
          allergies: "Allergies",
          offeredon: "9-5-2020",
          price: 1,
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
  });

  describe("UC-303 Meal overview", () => {
    beforeEach((done) => {
      pool.query(CLEAR_MEALS, (err, rows) => {
        if (err) {
          done(err);
        }
        if (rows) {
          pool.query(INSERT_MEALS, (err, rows) => {
            if (err) {
              done(err);
            }
            if (rows) {
              done();
            }
          });
        }
      });
    });

    afterEach((done) => {
      pool.query(CLEAR_MEALS, (err, rows) => {
        if (err) {
          done(err);
        }
        if (rows) {
          done();
        }
      });
    });

    it("(TC-303-1) returned een maaltijd", (done) => {
      chai
        .request(server)
        .get("/api/studenthome/7/meal")

        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
  });

  describe("UC-304 Meal details", () => {
    beforeEach((done) => {
      pool.query(CLEAR_MEALS, (err, rows) => {
        if (err) {
          done(err);
        }
        if (rows) {
          pool.query(INSERT_MEALS, (err, rows) => {
            if (err) {
              done(err);
            }
            if (rows) {
              done();
            }
          });
        }
      });
    });

    afterEach((done) => {
      pool.query(CLEAR_MEALS, (err, rows) => {
        if (err) {
          done(err);
        }
        if (rows) {
          done();
        }
      });
    });

    it("(TC-304-1 check of maaltijd bestaat)", (done) => {
      chai
        .request(server)
        .get(`'/api/studenthome/7/meal/999'`)

        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.be.a("object");
          done();
        });
    });

    it("(TC-304-2) Stuurt maaltijd terug", (done) => {
      chai
        .request(server)
        .get("/api/studenthome/7/meal/3")

        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
  });

  describe("UC-305 Delete meal", () => {
    beforeEach((done) => {
      pool.query(CLEAR_MEALS, (err, rows) => {
        if (err) {
          done(err);
        }
        if (rows) {
          pool.query(INSERT_MEALS, (err, rows) => {
            if (err) {
              done(err);
            }
            if (rows) {
              done();
            }
          });
        }
      });
    });

    afterEach((done) => {
      pool.query(CLEAR_MEALS, (err, rows) => {
        if (err) {
          done(err);
        }
        if (rows) {
          done();
        }
      });
    });

    it("(TC-305-1) Niet alle input gegevens", (done) => {
      chai
        .request(server)
        .delete("/api/studenthome/7/meal")
        .set(
          "authorization",
          "Bearer " + jwt.sign({ email: "test@test.nl" }, "secret")
        )

        .end((err, res) => {
          res.should.have.status(404);
          res.body.should.be.a("object");
          done();
        });
    });

    it("(TC-305-2) Checks of gebruiker ingelogd is", (done) => {
      chai
        .request(server)
        .delete("/api/studenthome/7/meal/3")

        .end((err, res) => {
          res.should.have.status(401);
          res.body.should.be.a("object");
          done();
        });
    });

    it("(TC-305-3) Checkt of gebruiker eigenaar van de meal is", (done) => {
      chai
        .request(server)
        .delete("/api/studenthome/7/meal/3")
        .set("authorization", "Bearer " + jwt.sign({ id: 8 }, "secret1"))

        .end((err, res) => {
          res.should.have.status(401);
          res.body.should.be.a("object");
          done();
        });
    });

    it("(TC-305-4) Check of maaltijd bestaat", (done) => {
      chai
        .request(server)
        .delete("/api/studenthome/7/meal/9999")
        .set("authorization", "Bearer " + jwt.sign({ id: 5 }, "secret"))

        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });

    it("(TC-305-5) Checked of maaltijd succesvol is ingeladen", (done) => {
      chai
        .request(server)
        .delete(`/api/studenthome/7/meal/3`)
        .set("authorization", "Bearer " + jwt.sign({ id: 7 }, "secret"))

        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
  });
});
