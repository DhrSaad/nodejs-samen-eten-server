process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";
console.log(`Running tests using database '${process.env.DB_DATABASE}'`);

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../index");
const pool = require("../config/database");
const jwt = require("jsonwebtoken");

chai.should();
chai.use(chaiHttp);
const INSERT_USER =
  "INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES" +
  '("first", "last", "name@server.nl","1234567", "secret");';
const INSERT_HOME =
  "INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES" +
  '("Goirkeplace", "Goirkestraat", "65", 1, "5049CA", "0687193417", "Tilburg");';
const CLEAR_HOME = "DELETE IGNORE FROM `studenthome`;";
const CLEAR_USER = "DELETE IGNORE FROM `user`;";
const SELECT = "SELECT * FROM `studenthome`";
const CLEAR_DB =
  CLEAR_HOME +
  CLEAR_USER +
  "ALTER TABLE `user` AUTO_INCREMENT = 1;" +
  "ALTER TABLE `studenthome` AUTO_INCREMENT = 1;";
describe("studenthome", () => {
  before((done) => {
    pool.query(CLEAR_DB, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        pool.query(INSERT_USER, (err, rows, fields) => {
          if (err) {
            done(err);
          } else if (rows) {
            pool.query(INSERT_HOME, (err, rows, fields) => {
              if (err) {
                done(err);
              } else {
                done();
              }
            });
          }
        });
      }
    });
  });

  describe("UC20X House", () => {
    it("TC-201-1 Verplicht veld ontbreekt", (done) => {
      chai
        .request(server)
        .post("/api/studenthome")
        .send({
          name: "Goirkeplace",
          address: "Goirkestraat",
          housenumber: "65",
          postcode: "5049CA",
          city: "Tilburg",
          userid: "1",
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .end((err, res) => {
          res.should.have.status(422);
          res.body.should.be.a("object");
          const response = res.body;
          response.should.have.property("error");
          // response.should.have.property('username').which.is.a('string')
          done();
        });
    });

    it("TC-201-2 Invalide postcode", (done) => {
      chai
        .request(server)
        .post("/api/studenthome")
        .send({
          name: "Goirkeplace",
          address: "Goirkestraat",
          housenumber: "65",
          phonenumber: "0687193417",
          postcode: 123,
          city: "Tilburg",
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.have.property("error");
          done();
        });
    });

    it("TC-201-3 Invalide telefoonnummer", (done) => {
      chai
        .request(server)
        .post("/api/studenthome")
        .send({
          name: "Goirkeplace",
          address: "Goirkestraat",
          housenumber: "65",
          phonenumber: 123,
          postcode: "5049CA",
          city: "Tilburg",
          userid: "1",
        })
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .end((err, res) => {
          res.should.have.status(422);
          const response = res.body;
          response.should.have.property("error");
          done();
        });
    });
  });
  it("TC-201-4 Studentenhuis bestaat alopdit adres (bestaandpostcode/huisnummer)", (done) => {
    pool.query(CLEAR_HOME + INSERT_HOME, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        chai
          .request(server)
          .post("/api/studenthome")
          .send({
            name: "Goirkeplace",
            address: "Goirkestraat",
            housenumber: "65",
            phonenumber: "0687193417",
            postcode: "5049CA",
            city: "Tilburg",
            userid: "1",
          })
          .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
          .end((err, res) => {
            res.should.have.status(500);
            res.body.should.be.a("object");
            const response = res.body;
            response.should.have.property("error");
            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-201-5 Niet Ingelogd", (done) => {
    chai
      .request(server)
      .post("/api/studenthome")
      .send({
        name: "Goirkeplace",
        address: "Goirkestraat",
        housenumber: "65",
        phonenumber: "0687193417",
        postcode: "5049CA",
        city: "Tilburg",
        userid: "1",
      })
      .end((err, res) => {
        res.should.have.status(401);
        res.body.should.be.a("object");
        const response = res.body;
        response.should.have.property("error");
        // response.should.have.property('username').which.is.a('string')
        done();
      });
  });
  it("TC-201-6 Studentenhuis  succesvol toegevoegd", (done) => {
    pool.query(CLEAR_HOME, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        chai
          .request(server)
          .post("/api/studenthome")
          .send({
            name: "Goirkeplace",
            address: "Goirkestraat",
            housenumber: "65",
            phonenumber: "0687193417",
            postcode: "5049CA",
            city: "Tilburg",
            userid: "1",
          })
          .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            const response = res.body;
            console.log(res.body);
            response.should.be
              .an("object")
              .that.has.all.keys("message", "result");

            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-202-1 Toon nul studentenhuizen", (done) => {
    pool.query(CLEAR_HOME, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        chai
          .request(server)
          .get("/api/studenthome")
          .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
          .end((err, res) => {
            res.should.have.status(200);
            res.body.result.should.be.empty;
            const response = res.body;
            console.log(res.body);

            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-202-2 Toon twee studentenhuizen", (done) => {
    pool.query(CLEAR_HOME + INSERT_HOME, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        chai
          .request(server)
          .get("/api/studenthome")
          .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            const response = res.body;
            response.should.have.property("result").which.is.a("array");
            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-202-3 Toon studentenhuizen met zoekterm op niet-bestaande stad", (done) => {
    pool.query(CLEAR_HOME + INSERT_HOME, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        chai
          .request(server)
          .get("/api/studenthome")
          .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            const response = res.body;
            response.should.have.property("result").which.is.a("array");
            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-202-4 should return a error when providing invalid information", (done) => {
    chai
      .request(server)
      .put("/api/studenthome")
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
      .end((err, res) => {
        res.should.have.status(404);
        res.body.should.be.a("object");
        const response = res.body;
        response.should.have.property("error").which.is.a("string");
        // response.should.have.property('username').which.is.a('string')
        done();
      });
  });
  it("TC-202-5 Toon studentenhuizen met zoekterm op bestaande stad", (done) => {
    chai
      .request(server)
      .get("/api/studenthome")
      .send({
        studenthome: "Tilburg",
      })
      .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))

      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        const response = res.body;
        response.should.have.property("result").which.is.a("array");
        // response.should.have.property('username').which.is.a('string')
        done();
      });
  });
  it("TC-203-1 Studentenhuis-ID bestaat", (done) => {
    pool.query(CLEAR_HOME + INSERT_HOME, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        chai
          .request(server)
          .get("/api/studenthome/7")
          .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            const response = res.body;
            console.log(res.body);
            response.should.have.property("result").which.is.a("array");
            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-203-2 Studentenhuis-ID bestaat niet", (done) => {
    pool.query(CLEAR_HOME, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        chai
          .request(server)
          .get("/api/studenthome/1")
          .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
          .end((err, res) => {
            res.should.have.status(412);
            res.body.should.be.a("object");
            const response = res.body;
            response.should.have.property("error");
            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-204-1 Verplicht veld ontbreekt", (done) => {
    pool.query(CLEAR_HOME + INSERT_HOME + SELECT, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        chai
          .request(server)
          .put("/api/studenthome/")
          .send({
            name: "Goirkepaleis",
            address: "Goirkestraat",
            housenumber: "65",
            phonenumber: "0687193417",
            postcode: "5049CA",
            city: "Tilburg",
            userid: "1",
          })
          .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
          .end((err, res) => {
            res.should.have.status(404);
            res.body.should.be.a("object");
            const response = res.body;
            console.log(res.body);
            response.should.have
              .property("error")
              .that.equals("Endpoint does not exist!");
            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-204-2 Invalide postcode", (done) => {
    pool.query(CLEAR_HOME + INSERT_HOME + SELECT, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        console.log(rows);
        chai
          .request(server)
          .put("/api/studenthome/10")
          .send({
            name: "Goirkepaleis",
            address: "Goirkestraat",
            housenumber: "65",
            phonenumber: "0687193417",
            postcode: 123,
            city: "Tilburg",
            userid: "1",
          })
          .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
          .end((err, res) => {
            res.should.have.status(401);
            res.body.should.be.a("object");
            console.log(res.body);
            const response = res.body;
            response.should.have.property("error");
            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-204-3 Invalide telefoonnummer", (done) => {
    pool.query(CLEAR_HOME + INSERT_HOME + SELECT, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        console.log(rows);
        chai
          .request(server)
          .put("/api/studenthome/10")
          .send({
            name: "Goirkepaleis",
            address: "Goirkestraat",
            housenumber: "65",
            phonenumber: 06,
            postcode: "5049CA",
            city: "Tilburg",
            userid: "1",
          })
          .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
          .end((err, res) => {
            res.should.have.status(422);
            res.body.should.be.a("object");
            console.log(res.body);
            const response = res.body;
            response.should.have.property("error");
            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-204-4 Studentenhuis bestaat niet ", (done) => {
    pool.query(CLEAR_HOME + SELECT, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        console.log(rows);
        chai
          .request(server)
          .put("/api/studenthome/10")
          .send({
            name: "Goirkepaleis",
            address: "Goirkestraat",
            housenumber: "65",
            phonenumber: 06,
            postcode: "5049CA",
            city: "Tilburg",
            userid: "1",
          })
          .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
          .end((err, res) => {
            res.should.have.status(422);
            res.body.should.be.a("object");
            console.log(res.body);
            const response = res.body;
            response.should.have.property("error");
            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-204-5 Niet ingelogd", (done) => {
    pool.query(CLEAR_HOME + INSERT_HOME, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        console.log(rows);
        chai
          .request(server)
          .put("/api/studenthome/10")
          .send({
            name: "Goirkepaleis",
            address: "Goirkestraat",
            housenumber: "65",
            phonenumber: "0687193417",
            postcode: "5049CA",
            city: "Tilburg",
            userid: "1",
          })
          .end((err, res) => {
            res.should.have.status(401);
            res.body.should.be.a("object");
            console.log(res.body);
            const response = res.body;
            response.should.have.property("error");
            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-204-6 Studentenhuis  succesvol gewijzigd", (done) => {
    pool.query(CLEAR_HOME + INSERT_HOME + SELECT, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        console.log(rows);
        chai
          .request(server)
          .put("/api/studenthome/12")
          .send({
            name: "Goirkepaleis",
            address: "Goirkestraat",
            housenumber: "65",
            phonenumber: "0687193417",
            postcode: "5049CA",
            city: "Tilburg",
            userid: "1",
          })
          .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            console.log(res.body);
            const response = res.body;
            response.should.have.property("result");
            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-205-1 Studentenhuis bestaat niet", (done) => {
    pool.query(CLEAR_HOME + INSERT_HOME + SELECT, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        console.log(rows);
        chai
          .request(server)
          .delete("/api/studenthome/13")
          .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
          .end((err, res) => {
            res.should.have.status(401);
            res.body.should.be.a("object");
            console.log(res.body);
            const response = res.body;
            response.should.have.property("error");
            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-205-2 Niet ingelogd", (done) => {
    pool.query(CLEAR_HOME + INSERT_HOME + SELECT, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        console.log(rows);
        chai
          .request(server)
          .delete("/api/studenthome/14")
          .end((err, res) => {
            res.should.have.status(401);
            res.body.should.be.a("object");
            console.log(res.body);
            const response = res.body;
            response.should.have.property("error");
            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-205-3 Actor is geen eigenaar", (done) => {
    pool.query(CLEAR_HOME + INSERT_HOME + SELECT, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        console.log(rows);
        chai
          .request(server)
          .delete("/api/studenthome/14")
          .end((err, res) => {
            res.should.have.status(401);
            res.body.should.be.a("object");
            console.log(res.body);
            const response = res.body;
            response.should.have.property("error");
            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
  it("TC-205-4 Studentenhuis  succesvol verwijderd", (done) => {
    pool.query(CLEAR_HOME + INSERT_HOME + SELECT, (err, rows, fields) => {
      if (err) {
        done(err);
      }
      if (rows) {
        console.log(rows);
        chai
          .request(server)
          .delete("/api/studenthome/16")
          .send({ userid: "1" })
          .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            console.log(res.body);
            const response = res.body;
            response.should.have.property("message");
            // response.should.have.property('username').which.is.a('string')
            done();
          });
      }
    });
  });
});
