process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";
console.log(`Running tests using database '${process.env.DB_DATABASE}'`);

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../index");
const pool = require("../config/database");

chai.should();
chai.use(chaiHttp);
const INSERT_USER =
  "INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES" +
  '("first", "last", "name@server.nl","1234567", "secret");';

const CLEAR_DB = "DELETE IGNORE FROM `user`";
describe("Authentication", () => {
  before((done) => {
    // console.log('beforeEach')
    pool.query(CLEAR_DB, (err, rows, fields) => {
      if (err) {
        console.log(`beforeEach CLEAR error: ${err}`);
        done(err);
      } else {
        done();
      }
    });
  });

  // After successful register we have a valid token. We export this token
  // for usage in other testcases that require login.
  // let validToken

  describe("UC101 Registation", () => {
    it("TC-101-1 Verplicht veld ontbreekt", (done) => {
      chai
        .request(server)
        .post("/api/register")
        .send({
          lastname: "LastName",
          email: "test@test.nl",
          studentnr: 1234567,
          password: "secret",
        })
        .end((err, res) => {
          res.should.have.status(412);
          const response = res.body;
          res.body.should.have.property("error").which.is.a("string");
          done();
        });
    });
    it("TC-101-2 Invalide email adres", (done) => {
      chai
        .request(server)
        .post("/api/register")
        .send({
          firstname: "FirstName",
          lastname: "LastName",
          email: 123,
          studentnr: 1234567,
          password: "secret",
        })
        .end((err, res) => {
          res.should.have.status(412);
          res.body.should.be.a("object");
          const response = res.body;
          response.should.have.property("error").which.is.a("string");
          done();
        });
    });
    it("TC-101-3 Invalide wachtwoord", (done) => {
      chai
        .request(server)
        .post("/api/register")
        .send({
          firstname: "FirstName",
          lastname: "LastName",
          email: "test@test.nl",
          studentnr: 1234567,
          password: 123,
        })
        .end((err, res) => {
          res.should.have.status(412);
          res.body.should.be.a("object");
          const response = res.body;
          response.should.have.property("error").which.is.a("string");
          done();
        });
    });
    it("TC-101-4 Gebruiker bestaat al", (done) => {
      chai.request(server).post("/api/register").send({
        firstname: "FirstName",
        lastname: "LastName",
        email: "test@test.nl",
        studentnr: 1234567,
        password: "secret",
      });

      chai
        .request(server)
        .post("/api/register")
        .send({
          firstname: "FirstName",
          lastname: "LastName",
          email: "test@test.nl",
          studentnr: 1234567,
        })

        .end((err, res) => {
          res.should.have.status(412);
          res.body.should.be.a("object");
          const response = res.body;
          response.should.have.property("error");
          done();
        });
    });

    it("TC-101-5 Gebruiker succesvol geregistreerd", (done) => {
      chai
        .request(server)
        .post("/api/register")
        .send({
          firstname: "FirstName",
          lastname: "LastName",
          email: "test@test.nl",
          studentnr: 1234567,
          password: "secret",
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          const response = res.body;
          response.should.have.property("token").which.is.a("string");
          // response.should.have.property('username').which.is.a('string')
          done();
        });
    });
  });
  describe("UC102 Login", () => {
    /**
     * This assumes that a user with given credentials exists. That is the case
     * when register has been done before login.
     */
    it("TC-102-1 Verplicht veld ontbreekt", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          email: "test@test.nl",
        })
        .end((err, res) => {
          res.should.have.status(422);
          res.body.should.be.a("object");
          const response = res.body;
          response.should.have.property("error").which.is.a("string");
          // response.should.have.property('username').which.is.a('string')
          done();
        });
    });
    it("TC-102-2 Invalide Email", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          email: 123,
          password: "",
        })
        .end((err, res) => {
          res.should.have.status(422);
          res.body.should.be.a("object");
          const response = res.body;
          response.should.have.property("error").which.is.a("string");
          // response.should.have.property('username').which.is.a('string')
          done();
        });
    });
    it("TC-102-3 Invalide Wachtwoord", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          email: "test@test.nl",
          password: 1234,
        })
        .end((err, res) => {
          res.should.have.status(422);
          res.body.should.be.a("object");
          const response = res.body;
          response.should.have.property("error").which.is.a("string");
          // response.should.have.property('username').which.is.a('string')
          done();
        });
    });
    it("TC-102-4 Gebruiker bestaat niet", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          email: "test@test.n XAZCZl",
          password: "secret",
        })
        .end((err, res) => {
          res.should.have.status(401);
          res.body.should.be.a("object");
          const response = res.body;
          response.should.have.property("error").which.is.a("string");
          // response.should.have.property('username').which.is.a('string')
          done();
        });
    });
    it("TC-102-5 Succesful Login", (done) => {
      pool.query(INSERT_USER, (error, result) => {
        if (error) {
          done(error);
        }
        if (result) {
          chai
            .request(server)
            .post("/api/login")
            .send({
              email: "name@server.nl",
              password: "secret",
            })
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a("object");
              const response = res.body;
              response.should.have.property("token").which.is.a("string");
              // response.should.have.property('username').which.is.a('string')
              done();
            });
        }
      });
    });
  });
});
